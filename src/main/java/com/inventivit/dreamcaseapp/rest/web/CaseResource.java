package com.inventivit.dreamcaseapp.rest.web;

import com.inventivit.dreamcaseapp.entity.CaseEntity;
import com.inventivit.dreamcaseapp.exception.BadRequestException;
import com.inventivit.dreamcaseapp.service.CaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/cases")
public class CaseResource {

    public CaseService caseService;


    @Autowired
    public CaseResource(CaseService caseService) {
        this.caseService = caseService;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CaseEntity> createCase(@RequestBody CaseEntity caseEntity){
        if (caseEntity.getId() != null) {
            throw new BadRequestException("id must be null");
        }
        return new ResponseEntity<>(caseService.createCase(caseEntity), new HttpHeaders(), HttpStatus.CREATED);
    }

    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CaseEntity> updateCase(@RequestBody CaseEntity caseEntity){
        if (caseEntity.getId() == null) {
            throw new BadRequestException("id does not exist");
        }
        return new ResponseEntity<>(caseService.updateCase(caseEntity), new HttpHeaders(), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Boolean> deleteCase(@PathVariable Long id){
        caseService.deleteCase(id);
        return new ResponseEntity<>(Boolean.TRUE, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<CaseEntity> getCase(@PathVariable Long id){
        return new ResponseEntity<>(caseService.getCase(id), new HttpHeaders(), HttpStatus.OK);
    }


}
