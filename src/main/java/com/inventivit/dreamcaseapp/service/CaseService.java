package com.inventivit.dreamcaseapp.service;

import com.inventivit.dreamcaseapp.entity.CaseEntity;

import java.util.Optional;

public interface CaseService {
    CaseEntity createCase(CaseEntity caseEntity);
    CaseEntity updateCase(CaseEntity caseEntity);
    CaseEntity getCase(Long id);
    void deleteCase(Long id);
}
