package com.inventivit.dreamcaseapp.service.impl;

import com.inventivit.dreamcaseapp.entity.CaseEntity;
import com.inventivit.dreamcaseapp.exception.NotFoundException;
import com.inventivit.dreamcaseapp.repository.CaseRepository;
import com.inventivit.dreamcaseapp.service.CaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
@Transactional
public class CaseServiceImpl implements CaseService {

    public CaseRepository caseRepository;

    @Autowired
    public CaseServiceImpl(CaseRepository caseRepository) {
        this.caseRepository = caseRepository;
    }


    @Override
    public CaseEntity createCase(CaseEntity caseEntity) {
        return caseRepository.save(caseEntity);
    }

    @Override
    public CaseEntity updateCase(CaseEntity caseEntity) {
        return caseRepository.save(caseEntity);
    }

    @Override
    public CaseEntity getCase(Long id) {
        Optional<CaseEntity>  caseEntity = caseRepository.findById(id);
        if (caseEntity.isEmpty()){
            throw new NotFoundException("case with id : "+ id +" not found on the database");
        }
        return caseEntity.get();
    }

    @Override
    public void deleteCase(Long id) {
        caseRepository.delete(getCase(id));
    }
}
