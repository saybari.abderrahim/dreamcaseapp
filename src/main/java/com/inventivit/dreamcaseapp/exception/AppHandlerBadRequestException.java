package com.inventivit.dreamcaseapp.exception;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class AppHandlerBadRequestException {

    @ExceptionHandler(value = {BadRequestException.class})
    public ResponseEntity<Object> handlerBadRequestException(BadRequestException badRequestException){

        ErrorMessage errorMessage = new ErrorMessage(badRequestException.getMessage());

        return new ResponseEntity<>(errorMessage, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }
}
