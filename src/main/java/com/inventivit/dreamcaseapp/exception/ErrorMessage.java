package com.inventivit.dreamcaseapp.exception;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class ErrorMessage implements Serializable {
    private static final long serialVersionUID = 0L;
    private final String message;
}
