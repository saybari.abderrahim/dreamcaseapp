package com.inventivit.dreamcaseapp.repository;

import com.inventivit.dreamcaseapp.entity.CaseEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CaseRepository extends JpaRepository<CaseEntity, Long> {
}
