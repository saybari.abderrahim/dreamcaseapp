package com.inventivit.dreamcaseapp.repository;

import com.inventivit.dreamcaseapp.entity.CaseEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.LocalDateTime;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@DataJpaTest
public class CaseRepositoryTest {

    @Autowired
    CaseRepository caseRepository;


    CaseEntity caseEntity;

    @BeforeEach
    void setUp() {
        this.caseEntity = CaseEntity.builder()
                .creationDate(LocalDateTime.now())
                .title("the first case")
                .description("case added in purpose of testing update operation")
                .lastUpdateDate(LocalDateTime.now())
                .build();

        this.caseEntity = this.caseRepository.save(caseEntity);

    }


    @Test
    void should_update_case() {

        this.caseEntity.setTitle("Updated Case");
        this.caseEntity.setDescription("Updated description");

        CaseEntity updatedCase = caseRepository.save(this.caseEntity);

        assertThat(updatedCase.getTitle()).isEqualTo("Updated Case");
        assertThat(updatedCase.getDescription()).isEqualTo("Updated description");
    }
}
