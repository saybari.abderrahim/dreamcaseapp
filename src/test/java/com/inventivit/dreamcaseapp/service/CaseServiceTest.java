package com.inventivit.dreamcaseapp.service;

import com.inventivit.dreamcaseapp.entity.CaseEntity;
import com.inventivit.dreamcaseapp.repository.CaseRepository;
import com.inventivit.dreamcaseapp.service.impl.CaseServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CaseServiceTest {

    @InjectMocks
    private CaseServiceImpl underTestCaseService;
    @Mock
    CaseRepository caseRepositoryMock;

    CaseEntity caseEntity;

    @BeforeEach
    void setUp() {
        this.caseEntity = CaseEntity.builder()
                .creationDate(LocalDateTime.now())
                .title("the first case")
                .description("case added in purpose of testing create operation")
                .lastUpdateDate(LocalDateTime.now())
                .build();

        this.setUpMock();
    }


    void setUpMock(){
        this.underTestCaseService.caseRepository = this.caseRepositoryMock;
    }


    @Test
    void should_create_case() {
        when(caseRepositoryMock.save(any(CaseEntity.class))).thenReturn(this.caseEntity);
        underTestCaseService.createCase(caseEntity);

        verify(caseRepositoryMock, times(1)).save(any(CaseEntity.class));

    }

}
